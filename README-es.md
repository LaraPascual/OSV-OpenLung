

Este documento en otros idiomas:
|[english](README.md)|[català](README-ca.md)|[français](README-fr.md)|[español](README-es.md)|[日本語](README-ja.md)|[汉语](README-zh-Hans.md)|[漢語](README-zh-Hant.md)|[português](README-pt_BR.md)|[nederlands](README-nl.md)|[Русский](README-ru.md)
|---|---|---|---|---|---|---|---|---|---|


Mascarilla [EN CURSO] de implementación rápida de código abierto que utiliza la bolsa Ambu-bag como componente central. El sistema utiliza microelectrónica para detectar y controlar de forma segura las características de presión y flujo, operando de forma semiautónoma. Únase al Git y eche un vistazo a las imágenes de diseño actuales.
